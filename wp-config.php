<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

$path = dirname(__FILE__);
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

if (file_exists(dirname(__FILE__) . '/config-staging.php')) {
	require 'config-staging.php';
} else if (file_exists(dirname(__FILE__) . '/config-production.php')) {
	require 'config-production.php';
} else if (file_exists(dirname(__FILE__) . '/config-development.php')) {
	require 'config-development.php';
} else {
	echo "No configuration file found.";
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'WrCFYk8G>Z1-(O-IAx?c.bX9xIP{Y>L.G:MO>a;!+J )lM>72[Wp1zQ7(uHSGC-Q');
define('SECURE_AUTH_KEY', '$iO>_J[8aB&4&&A;Qt,VS:+0N--+3JJm&0TZLy=-LC~7M[qksV;S[GWu^I<dK*Df');
define('LOGGED_IN_KEY', '|36OMqz;:--JU%ix)KLh(VX1zR:r3lYr=}6%]{+a-KMa7*9Di2|eKnnD_G(gx+`u');
define('NONCE_KEY', 'pN&-5kJ7aovic{[Nn0oaDW;:$A1 <,Oe0|UE<r(Op[RQE-mK7qtEY5f#eZrtJ8|+');
define('AUTH_SALT', 'bQ3+|^4227S:d@1o}o_7oJ2/THE9hQe;wQ9+|W:?^iX9?%Z^5&P(m9`po/<H7md-');
define('SECURE_AUTH_SALT', 'a(4}e^jb84Zh0}sr:lIVS65Y3VR$etSVUBcB;Y/[8Ib+kz|,kLL$-rGawxuDst^e');
define('LOGGED_IN_SALT', 'f@PMaa?TX||Z%E|{XAS`e.!+JUi~v8Iwi_2kfg@3X0,gN:Sp5{DO}y0r?6}WEkkw');
define('NONCE_SALT', '|/b5iml5gi^[JQQW1hg~|t#rqF]!yk~hirS3Ile)^Jf_U_3S@1TH2vRpdh!FY+-.');

/**#@-*/

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

// wp home
define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']);

// // define the wp root
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp');

define('CONTENT_DIR', '/wp-content');
define('WP_CONTENT_DIR', dirname(__FILE__) . CONTENT_DIR);
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . CONTENT_DIR);

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', dirname(__FILE__) . '/wp');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
