set :stage, :development

set :user, 'jonwoh'

server 'wilmic.wohllabs.com', user: fetch(:user), roles: %w{web app}

set :tmp_dir, '/home/jonwoh/tmp'
set :deploy_to, '/home/jonwoh/wilmic.wohllabs.com'
set :linked_files, %w{config-development.php}
set :branch, "master"

set :live_domain, 'wilmic.wohllabs.com'
set :db_name, 'wp_wilmic'
set :db_user, 'wp_wilmic'
set :db_pass, 'VEl31t3%}5U6H}v'
set :db_host, 'db.wohllabs.com'