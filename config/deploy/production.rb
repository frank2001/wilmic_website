set :stage, :production

set :user, 'jwohl'

server 'wilmic.com', user: fetch(:user), roles: %w{web app}

set :ssh_options, {
  forward_agent: true,
  port: 9560
}

set :tmp_dir, '/home/jwohl/tmp'
set :deploy_to, '/var/staging.wilmic.net'
set :linked_files, %w{config-staging.php}
set :branch, "master"

set :live_domain, 'staging.wilmic.net'
set :db_name, 'wp_wilmic'
set :db_user, 'wp_wilmic'
set :db_pass, 'VEl31t3%}5U6H}v'
set :db_host, 'localhost'