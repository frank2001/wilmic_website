set :application, 'WILMIC'
set :repo_url, 'jmwgitolite@carousel.dreamhost.com:wilmic.wohllabs.com'

set :scm, :git
set :ssh_options, { :forward_agent => true }

set :deploy_via, :remote_cache
set :copy_exclude, [".git", ".DS_Store", ".gitignore", ".gitmodules"]

set :linked_dirs, %w{wp-content/uploads}

set :wpcli_local_url, 'local.wilmic.com'

# This may or may not be necessary depending on how composer is linked
# SSHKit.config.command_map[:composer] = "/path/to/composer"
# set :default_environment, {
#   'PATH' => "/home/jonwoh/opt/bin:/usr/local/php53/bin:$PATH"
# }

set :composer_install_flags, '--no-dev --prefer-dist --no-scripts --quiet --optimize-autoloader'
set :composer_roles, :all


desc "Check that we can access everything"
task :check_write_permissions do
  on roles(:all) do |host|
    if test("[ -w #{fetch(:deploy_to)} ]")
      info "#{fetch(:deploy_to)} is writable on #{host}"
    else
      error "#{fetch(:deploy_to)} is not writable on #{host}"
    end
  end
end

# check remote server settings
desc "Check remote server settings"
task :servercheck do
  on roles(:all) do |h|
    info "Checking PHP Version..."
    execute :php, '-v'
    
    info "Checking write access..."
    if test("[ -w #{fetch(:deploy_to)} ]")
      info "#{fetch(:deploy_to)} is writable on #{h}"
    else
      error "#{fetch(:deploy_to)} is not writable on #{h}"
    end
  end
end



# copy local db to dev
desc "Migrate local db and assets to remote server."
namespace :migrate do
  
  task :localdb do
    on roles(:all) do |h|
      info "import and update database.sql"
      execute :mysql, "-u#{fetch:db_user} -p#{fetch:db_pass} #{fetch:db_name} -h #{fetch:db_host} < #{release_path}/sql/database.sql"
      execute :srdb, "-h #{fetch:db_host} -n #{fetch:db_name} -u #{fetch:db_user} -p #{fetch:db_pass} -s 'local.wilmic.com' -r '#{fetch:live_domain}'"
    end
  end


  ##
  # Usage: 
  # cap staging migrate:update_links[string_to_replace,dry]
  # 
  # Example (dry run):
  # cap staging migrate:update_links[local.wilmic.com,dry]
  # 
  # Example (actual run):
  # cap staging migrate:update_links[local.wilmic.com]
  ##
  task :update_links, :search, :dry do |t, args|
    on roles(:all) do |h|
      info "updating absolute urls, replacing #{args[:search]} with #{fetch:live_domain}"
      dry = args[:dry] ? '--dry-run' : ''
      cmd = "search-replace", "'#{args[:search]}' '#{fetch:live_domain}' #{dry}" 
      execute :wp, "search-replace", "--path=#{release_path}/wp", "'#{args[:search]}' '#{fetch:live_domain}'", dry
    end
  end

  task :localassets do
    upload_dir = "uploads"
    upload_ts = Time.now.getutc.to_i.to_s
    
    run_locally do 
        info "tar local assets"
        # execute :rm, '-rf', upload_dir+".tar.gz"
        execute :tar, 'zcf', upload_dir+".tar.gz", "wp-content/"+upload_dir
    end

    on roles(:all) do |h|
      info "upload assets"
      upload! upload_dir+".tar.gz", "#{shared_path}/wp-content"
      
      if test("[ -d #{shared_path}/wp-content/"+upload_dir+" ]")
        # the file exists
        info "backup current assets to #{shared_path}/wp-content/"+upload_dir+"-"+upload_ts
        execute :mv, "#{shared_path}/wp-content/"+upload_dir, "#{shared_path}/wp-content/"+upload_dir+"-"+upload_ts
      end

      info "expand new assets"
      execute :tar, 'zxf', "#{shared_path}/wp-content/"+upload_dir+".tar.gz", "-C", "#{shared_path}/wp-content/", '--strip-components=1'

      info "rm archive"
      execute :rm, "#{shared_path}/wp-content/"+upload_dir+".tar.gz"
    end

    run_locally do 
        info "remove local assets tar"
        execute :rm, '-rf', upload_dir+".tar.gz"
    end
  end
end

namespace :wp do

  desc 'Generate .htaccess file'
  task :htaccess do
    on roles(:all) do |h|
      execute :cp, "#{release_path}/htaccess.txt", "#{release_path}/.htaccess"
      execute :sed, '-i', "'s/YOUR_DOMAIN/#{fetch:live_domain}/g' #{release_path}/.htaccess"
      execute :rm, "#{release_path}/htaccess.txt"
    end
  end

  desc 'Generate environment wp config file'
  task :wp_config do
    run_locally do 
      execute :cp, "config.php.txt", "config-#{fetch:stage}.php"
      execute :sed, '-i ""', "'s/\$DB_NAME/#{fetch:db_name}/g' config-#{fetch:stage}.php"
      execute :sed, '-i ""', "'s/\$DB_USER/#{fetch:db_user}/g' config-#{fetch:stage}.php"
      execute :sed, '-i ""', "'s/\$DB_PASS/#{fetch:db_pass}/g' config-#{fetch:stage}.php"
      execute :sed, '-i ""', "'s/\$DB_HOST/#{fetch:db_host}/g' config-#{fetch:stage}.php"
    end

    # on roles(:all) do |h|
    #   execute :cp, "#{shared_path}/htaccess.txt", "#{release_path}/.htaccess"
    #   execute :sed, '-i', "'s/YOUR_DOMAIN/#{fetch:live_domain}/g' #{release_path}/.htaccess"
    #   execute :rm, "#{release_path}/htaccess.txt"
    # end
  end
end

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finishing, 'deploy:cleanup'

end

# after deploy
after :deploy, 'wp:htaccess'
