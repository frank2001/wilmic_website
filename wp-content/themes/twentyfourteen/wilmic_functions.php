
<?php

/************* ADMIN CONFIG *****************/

function remove_menus(){
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );


/************* CUSTOM TYPES *****************/

// STAFF
function staff_post_type() {

	$labels = array(
		'name'                => _x( 'Staff Members', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Staff Member', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Staff Members', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Staff Members', 'text_domain' ),
		'view_item'           => __( 'View Staff Member', 'text_domain' ),
		'add_new_item'        => __( 'Add New Staff Member', 'text_domain' ),
		'add_new'             => __( 'New Staff Member', 'text_domain' ),
		'edit_item'           => __( 'Edit Staff Member', 'text_domain' ),
		'update_item'         => __( 'Update Staff Member', 'text_domain' ),
		'search_items'        => __( 'Search staff members', 'text_domain' ),
		'not_found'           => __( 'No staff members found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No staff members found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'staff', 'text_domain' ),
		'description'         => __( 'Staff Member', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'staff', $args );

}
add_action( 'init', 'staff_post_type', 0 );

// BOARD
function board_post_type() {

	$labels = array(
		'name'                => _x( 'Board Members', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Board Member', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Board Members', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Board Members', 'text_domain' ),
		'view_item'           => __( 'View Board Member', 'text_domain' ),
		'add_new_item'        => __( 'Add New Board Member', 'text_domain' ),
		'add_new'             => __( 'New Board Member', 'text_domain' ),
		'edit_item'           => __( 'Edit Board Member', 'text_domain' ),
		'update_item'         => __( 'Update Board Member', 'text_domain' ),
		'search_items'        => __( 'Search board members', 'text_domain' ),
		'not_found'           => __( 'No board members found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No board members found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'board', 'text_domain' ),
		'description'         => __( 'Board Member', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'board', $args );

}
add_action( 'init', 'board_post_type', 1 );

// ARTICLES
function article_post_type() {

	$labels = array(
		'name'                => _x( 'Articles', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Article', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Articles', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Articles', 'text_domain' ),
		'view_item'           => __( 'View Article', 'text_domain' ),
		'add_new_item'        => __( 'Add New Article', 'text_domain' ),
		'add_new'             => __( 'New Article', 'text_domain' ),
		'edit_item'           => __( 'Edit Article', 'text_domain' ),
		'update_item'         => __( 'Update Article', 'text_domain' ),
		'search_items'        => __( 'Search articles', 'text_domain' ),
		'not_found'           => __( 'No articles found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No articles found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'article', 'text_domain' ),
		'description'         => __( 'Article', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'article', $args );

}
add_action( 'init', 'article_post_type', 1 );

?>