<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="first clearfix" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

							
								<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<!-- <p class="byline vcard"><?php
										printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'bonestheme' ) ), bones_get_the_author_posts_link());
									?></p> -->
									<?php if(get_field('page_intro_text')): ?>
										<div class="featured">
											<?php echo get_field('page_intro_text'); ?>
										</div>
									<?php endif; ?>
									<?php if(get_field('page_intro_image')): ?>
										<div class="featured-image">
											<img src="<?php $image = get_field('page_intro_image'); echo $image['url']; ?>" alt="Content Banner Image" />
										</div>
									<?php endif; ?>

								</header>

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>

									<?php if(get_field('newsletters')): ?>
										<div id="Newsletters">
											<h4>Get the Latest Issue:</h4>
											<?php
												$repeater = get_field('newsletters');
												$latestIssue = $repeater[0];
												$year = $latestIssue['year'];
												$title = $latestIssue['title'];
												$file = $latestIssue['newsletter_document'];
											?>

											<?php echo do_shortcode('[action_link icon="/wp-content/uploads/2014/03/document.png" 
												url="'.$file['url'].'" sub_text=""]'.$title.'[/action_link]'); ?>
											
											<!-- <a href="<?php echo $file['url']; ?>"><?php echo $title; ?></a> -->

											<h4>Archive</h4>
											<select id="NewsletterDropdown">
												<option value="">Select...</option>
												<?php while(the_repeater_field('newsletters')): ?>
													<?php $year = get_sub_field('year'); ?>
													<?php $title = get_sub_field('title'); ?>
													<?php $file = get_sub_field('newsletter_document'); ?>
													
													<option value="<?php echo $file['url']; ?>"><?php echo $title; ?></option>
													<!-- <div class="podcasts">
														<div class='desc'>
															<a href="<?php echo $file['url']; ?>" target="_blank"><?php echo $title; ?></a>
														</div>
													</div> -->
											    <?php endwhile; ?>
											</select>
										</div>
									<?php endif; ?>
								
								</section>

								<footer class="article-footer">
									<?php the_tags( '<span class="tags">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?>

								</footer>

								<!-- <?php comments_template(); ?> -->

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
