<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // Google Chrome Frame for IE ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/library/less/slicknav.css">
		<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/library/js/jquery.slicknav.min.js"></script>
		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?>>

		<?php if(get_field('images')): ?>
			<div id="SliderWrap">
				<div id="Slider">
					<?php while(the_repeater_field('images')): ?>
						<?php 
							$image = get_sub_field('image'); 
							$text = get_sub_field('text');
							$subtext = get_sub_field('sub-text');
							$pos = get_sub_field('text-position') ? (get_sub_field('text-position') - 400) . "px" : "-300px";
							$image_pos = get_sub_field('image_position');
							$link = get_sub_field('link');
							$link_text = get_sub_field('link_text');
							$enabled = get_sub_field('enabled');
						?>

						<?php if ($enabled): ?>
						<div class="slider-item">
							<div class="slider-image" style="background-image: url('<?php echo $image['url']; ?>'); background-position: <?php echo $image_pos; ?>;"></div>
							<div class="wrap">
								<div class="slider-text" style="top: <?php echo $pos; ?>">
									<div class="slider-maintext">
										<?php echo $text; ?>
									</div>
									<div class="slider-subtext">
										<?php echo $subtext; ?>
									</div>
									<?php if ($link_text) { ?>
										<div class="slider-link">
											<a class="action-link action-link-home" href="<?php echo $link; ?>">
												<div class="text no-icon">
													<div class="main_text"><?php echo $link_text; ?></div>
												</div>
												<div class="clearfix"></div>
											</a>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php endif; ?>

						 
				    <?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>

		<div id="container">
			

			<header class="header" role="banner">
				<div class="wrap top-wrap">
					<div class="search">
						<?php get_search_form(); ?>
					</div>

					<?php //wp_nav_menu( array( 'theme_location' => 'header-nav' ) ); ?>
				</div>
				
				<!-- <div class="wrap headev-nav-wrap">
					<?php wp_nav_menu( array( 'theme_location' => 'header-nav' ) ); ?>
				</div> -->

				<div id="inner-header" class="wide-wrap clearfix">
					<div id="logo" class="wrap">
						<a href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php echo get_bloginfo('template_directory');?>/library/images/WILMIC-Logo-blue.png" /></a>
					</div>
				</div>

				<nav class="wrap" role="navigation">
					<?php bones_main_nav(); ?>
				</nav>

			</header>
