<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="first clearfix" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<header class="article-header">

								<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
								<?php if(get_field('page_intro_text')): ?>
									<div class="featured">
										<?php echo get_field('page_intro_text'); ?>
									</div>
								<?php endif; ?>
								<?php if(get_field('page_intro_image')): ?>
									<div class="featured-image">
										<img src="<?php $image = get_field('page_intro_image'); echo $image['url']; ?>" alt="Content Banner Image" />
									</div>
								<?php endif; ?>
							</header>
							
							<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
							</section>

						<?php endwhile; endif; ?>
				
						<?php
							global $wp_query;
							$args = array( 'post_type' => 'event', 'posts_per_page' => 5, 'paged' => $wp_query->query_vars['paged'] );
							$loop = new WP_Query( $args );
							
						?>

							<?php if (have_posts()) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

							<?php $display_date = date('F d, Y', strtotime(get_field('date'))); ?>
							<div class="event-item">
								<h4><?php the_title(); ?></h4>
								<div class='desc'>
									<div class="date">
										<?php echo $display_date; ?>
									</div>
									<div class="location">
										<?php the_field('location'); ?>
									</div>
									<div class="excerpt">
										<?php the_excerpt(); ?>
									</div>
									<div class="actions">
										<a class="more-info" href="<?php the_permalink(); ?>" >More Info</a>
										<?php if (get_field('registration_link')) { ?>
											<a class="register-now" href="<?php the_field('registration_link'); ?>" target="_blank">Register Now &rarr;</a>
										<?php } ?>
									</div>
								</div>
							</div>

							<?php endwhile; ?>

							<?php if (function_exists('bones_page_navi')) { ?>
									<?php bones_page_navi($loop); ?>
							<?php } else { ?>
								<nav class="wp-prev-next">
										<ul class="clearfix">
											<li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'bonestheme' )) ?></li>
											<li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'bonestheme' )) ?></li>
										</ul>
								</nav>
							<?php } ?>


							<?php else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
