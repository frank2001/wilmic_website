<?php get_header(); ?>

			<div id="content">
				<?php if(get_field('banner_image')): ?>
					<?php $image = get_field('banner_image'); ?>
					<div class="content-image" style="background-image: url('<?php echo $image['url']; ?>');"></div>
				<?php endif; ?>

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="first clearfix" role="main">

						<header class="article-header">

							<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
							<!-- <p class="byline vcard"><?php
								printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'bonestheme' ) ), bones_get_the_author_posts_link());
							?></p> -->
							<?php if(get_field('page_intro_text')): ?>
								<div class="featured">
									<?php echo get_field('page_intro_text'); ?>
								</div>
							<?php endif; ?>
							<?php if(get_field('page_intro_image')): ?>
								<div class="featured-image">
									<img src="<?php $image = get_field('page_intro_image'); echo $image['url']; ?>" alt="Content Banner Image" />
								</div>
							<?php endif; ?>
							
							

						</header>
				


							<?php
								
								$args = array( 'post_type' => 'board', 'posts_per_page' => 15 );
								$loop = new WP_Query( $args );
								
							?>

							<?php if (have_posts()) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

							<div class="staff-member">
								<a href="<?php echo get_permalink(); ?>">
									<?php
										if ( has_post_thumbnail() ) {
											the_post_thumbnail(100, 100);									}
									?>
								</a>
								<div class="text">
									<h5><?php the_title(); ?></h5>
									<div class='desc'>
										<div>
											<?php the_field('title'); ?>
										</div>
										<div>
											<?php the_field('company'); ?>
										</div>
										<div>
											<?php the_field('location'); ?>
										</div>
										<a href="<?php echo get_permalink(); ?>">Bio</a>
									</div>
								</div>
							</div>

							<?php endwhile; else : ?>


									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
