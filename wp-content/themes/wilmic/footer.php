			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">
					<p class="footer-contact">
						<div><a href="mailto:<?php the_field('email_address', 'option'); ?>"><?php the_field('email_address', 'option'); ?></a></div>
						<br/>
						<div>p <?php the_field('toll_free_phone_number', 'option'); ?> | <?php the_field('local_phone_number', 'option'); ?></div>
						<div>f <?php the_field('fax_number', 'option'); ?></div>
					</p>
					<p>
						<div><?php the_field('street_address_1', 'option'); ?></div>
						<?php if (get_field('street_address_2', 'option')) { ?>
							<div><?php the_field('street_address_2', 'option'); ?></div>
						<?php } ?>
						<div><?php the_field('city', 'option'); ?>, <?php the_field('state', 'option'); ?> <?php the_field('zip', 'option'); ?></div>
					</p>
					<nav role="navigation">
							<?php bones_footer_links(); ?>
					</nav>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>

				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html>
