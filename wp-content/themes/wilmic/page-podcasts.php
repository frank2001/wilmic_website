<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="first clearfix" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

							
								<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<!-- <p class="byline vcard"><?php
										printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'bonestheme' ) ), bones_get_the_author_posts_link());
									?></p> -->


								</header>

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>

									<?php if(get_field('podcasts')): ?>
										<div id="Podcasts">
											<?php while(the_repeater_field('podcasts')): ?>
												<?php $title = get_sub_field('podcast_title'); ?>
												<?php $description = get_sub_field('podcast_description'); ?>
												<?php $date = get_sub_field('podcast_date'); ?>
												<?php $file = get_sub_field('podcast_file'); ?>
												
												<?php //print_r($file); die; ?>
												<div class="podcasts">
													<h4><?php echo $title; ?></h4>
													<div class='desc'>
														<div class="date">
															<?php echo $date; ?>
														</div>
														<div class="excerpt">
															<?php echo $description; ?>
														</div>
														<div class="player">
															<?php echo do_shortcode( '[audio url="' . $file['url'] . '"]' ); ?>
														</div>
														<a href="<?php echo $file['url']; ?>" target="_blank">Download MP3</a>
													</div>
												</div>
										    <?php endwhile; ?>
										</div>
									<?php endif; ?>
								
								</section>

								<footer class="article-footer">
									<?php the_tags( '<span class="tags">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?>

								</footer>

								<!-- <?php comments_template(); ?> -->

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
