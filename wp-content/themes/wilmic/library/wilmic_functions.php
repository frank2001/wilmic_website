<?php

/************* ADMIN CONFIG *****************/

// function remove_menus(){
//   remove_menu_page( 'edit-comments.php' );
// }
// add_action( 'admin_menu', 'remove_menus' );


/************* CUSTOM TYPES *****************/

// STAFF
function staff_post_type() {

	$labels = array(
		'name'                => _x( 'Staff Members', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Staff Member', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Staff Members', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Staff Members', 'text_domain' ),
		'view_item'           => __( 'View Staff Member', 'text_domain' ),
		'add_new_item'        => __( 'Add New Staff Member', 'text_domain' ),
		'add_new'             => __( 'New Staff Member', 'text_domain' ),
		'edit_item'           => __( 'Edit Staff Member', 'text_domain' ),
		'update_item'         => __( 'Update Staff Member', 'text_domain' ),
		'search_items'        => __( 'Search staff members', 'text_domain' ),
		'not_found'           => __( 'No staff members found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No staff members found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'staff', 'text_domain' ),
		'description'         => __( 'Staff Member', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'staff', $args );

}
add_action( 'init', 'staff_post_type', 0 );

// BOARD
function board_post_type() {

	$labels = array(
		'name'                => _x( 'Board Members', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Board Member', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Board Members', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Board Members', 'text_domain' ),
		'view_item'           => __( 'View Board Member', 'text_domain' ),
		'add_new_item'        => __( 'Add New Board Member', 'text_domain' ),
		'add_new'             => __( 'New Board Member', 'text_domain' ),
		'edit_item'           => __( 'Edit Board Member', 'text_domain' ),
		'update_item'         => __( 'Update Board Member', 'text_domain' ),
		'search_items'        => __( 'Search board members', 'text_domain' ),
		'not_found'           => __( 'No board members found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No board members found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'board', 'text_domain' ),
		'description'         => __( 'Board Member', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'board', $args );

}
add_action( 'init', 'board_post_type', 1 );

// ARTICLES
function article_post_type() {

	$labels = array(
		'name'                => _x( 'Articles', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Article', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Articles', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Articles', 'text_domain' ),
		'view_item'           => __( 'View Article', 'text_domain' ),
		'add_new_item'        => __( 'Add New Article', 'text_domain' ),
		'add_new'             => __( 'New Article', 'text_domain' ),
		'edit_item'           => __( 'Edit Article', 'text_domain' ),
		'update_item'         => __( 'Update Article', 'text_domain' ),
		'search_items'        => __( 'Search articles', 'text_domain' ),
		'not_found'           => __( 'No articles found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No articles found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'article', 'text_domain' ),
		'description'         => __( 'Article', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'article', $args );

}
add_action( 'init', 'article_post_type', 1 );

// RESOURCES
function resource_post_type() {

	$labels = array(
		'name'                => _x( 'Resources', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Resource', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Resources', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Resources', 'text_domain' ),
		'view_item'           => __( 'View Resource', 'text_domain' ),
		'add_new_item'        => __( 'Add New Resource', 'text_domain' ),
		'add_new'             => __( 'New Resource', 'text_domain' ),
		'edit_item'           => __( 'Edit Resource', 'text_domain' ),
		'update_item'         => __( 'Update Resource', 'text_domain' ),
		'search_items'        => __( 'Search resources', 'text_domain' ),
		'not_found'           => __( 'No resources found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No resources found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'resource', 'text_domain' ),
		'description'         => __( 'Resource', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'resource_category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'resource', $args );

}
add_action( 'init', 'resource_post_type', 1 );

function resource_taxonomy_init() {
	// create a new taxonomy
	register_taxonomy(
		'resource_category',
		'resource',
		array(
			'label' => __( 'Resource Category' ),
			'hierarchical' => true,
			'show_admin_column' => true
			// 'rewrite' => array( 'slug' => 'person' ),
			// 'capabilities' => array(
			// 	'assign_terms' => 'edit_guides',
			// 	'edit_terms' => 'publish_guides'
			// )
		)
	);
}
add_action( 'init', 'resource_taxonomy_init' );

// EVENTS
function event_post_type() {

	$labels = array(
		'name'                => _x( 'Events', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Events', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Events', 'text_domain' ),
		'view_item'           => __( 'View Event', 'text_domain' ),
		'add_new_item'        => __( 'Add New Event', 'text_domain' ),
		'add_new'             => __( 'New Event', 'text_domain' ),
		'edit_item'           => __( 'Edit Event', 'text_domain' ),
		'update_item'         => __( 'Update Event', 'text_domain' ),
		'search_items'        => __( 'Search events', 'text_domain' ),
		'not_found'           => __( 'No events found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No events found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'event', 'text_domain' ),
		'description'         => __( 'Event', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 20,
		// 'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'event_post_type', 1 );

// // MULTIMEDIA_ITEM
// function event_post_type() {

// 	$labels = array(
// 		'name'                => _x( 'Videos', 'Post Type General Name', 'text_domain' ),
// 		'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'text_domain' ),
// 		'menu_name'           => __( 'Events', 'text_domain' ),
// 		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
// 		'all_items'           => __( 'All Events', 'text_domain' ),
// 		'view_item'           => __( 'View Event', 'text_domain' ),
// 		'add_new_item'        => __( 'Add New Event', 'text_domain' ),
// 		'add_new'             => __( 'New Event', 'text_domain' ),
// 		'edit_item'           => __( 'Edit Event', 'text_domain' ),
// 		'update_item'         => __( 'Update Event', 'text_domain' ),
// 		'search_items'        => __( 'Search events', 'text_domain' ),
// 		'not_found'           => __( 'No events found', 'text_domain' ),
// 		'not_found_in_trash'  => __( 'No events found in Trash', 'text_domain' ),
// 	);
// 	$args = array(
// 		'label'               => __( 'event', 'text_domain' ),
// 		'description'         => __( 'Event', 'text_domain' ),
// 		'labels'              => $labels,
// 		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields'),
// 		'taxonomies'          => array( 'category', 'post_tag' ),
// 		'hierarchical'        => false,
// 		'public'              => true,
// 		'show_ui'             => true,
// 		'show_in_menu'        => true,
// 		'show_in_nav_menus'   => false,
// 		'show_in_admin_bar'   => false,
// 		'menu_position'       => 20,
// 		// 'menu_icon'           => '',
// 		'can_export'          => true,
// 		'has_archive'         => true,
// 		'exclude_from_search' => false,
// 		'publicly_queryable'  => true,
// 		'capability_type'     => 'page',
// 	);
// 	register_post_type( 'event', $args );

// }
// add_action( 'init', 'event_post_type', 1 );


// Add Shortcode
function action_link_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'url' => '#',
			'primary_text' => 'Link Text...',
			'secondary_text' => '',
		), $atts )
	);

	// Code
if ( isset( $url ) ) {
	return '<a class="action-link" rel="' . $secondary_text . '" href="' . $url . '"><span class="primary-text">' . $primary_text . '</span> <span class="secondary-text">' . $secondary_text . '</span></a>';
}

}
add_shortcode( 'action-link', 'action_link_shortcode' );


/**
 * TINY MCE FORMAT STYLES
 */

// enable styles menu in TinyMCE
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


// Add custom styles to Styles menu
function my_mce_before_init( $settings ) {

    $style_formats = array(
        array(
        	'title' => 'Featured Text',
        	'block' => 'div',
        	'classes' => 'featured',
        	'wrapper' => true
        ),
        array(
        	'title' => 'Action Link',
        	'selector' => 'a',
        	'classes' => 'action-link'
        )
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );

// Add custom stylesheet to TinyMCE so that the styles display there
function wilmic_add_editor_styles() {
    add_editor_style( 'library/css/custom-editor-style.css' );
}
add_action( 'init', 'wilmic_add_editor_styles' );








add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}




add_filter( 'manage_edit-resource_columns', 'my_edit_resource_columns' ) ;

function my_edit_resource_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Resource' ),
		'link' => __( 'Link' ),
		'resource_categories' => __( 'Categories' ),
		'date' => __( 'Date' )
	);

	return $columns;
}

add_action( 'manage_resource_posts_custom_column', 'my_manage_resource_columns', 10, 2 );

function my_manage_resource_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		/* If displaying the 'duration' column. */
		case 'link' :

			/* Get the post meta. */
			$link = get_field('link', $post_id);

			/* If no link is found, output a default message. */
			if ( empty( $link ) )
				echo __( 'Unknown' );

			/* If there is a duration, append 'minutes' to the text string. */
			else
				echo $link;

			break;
		case 'resource_categories':
			echo get_the_term_list($post_id,'resource_category','',', ','');
			break;
		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}


add_action( 'admin_menu', 'my_remove_menu_pages' );

function my_remove_menu_pages() {
    remove_menu_page('edit.php');
}



?>