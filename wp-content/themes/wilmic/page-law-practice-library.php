<?php get_header(); ?>

			<div id="content">

				<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/library/js/libs/chosen_v1.1.0/chosen.css">

				<div id="inner-content" class="wrap clearfix">

						<div id="main-lpl" class="first clearfix" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<!-- <p class="byline vcard"><?php
										printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'bonestheme' ) ), bones_get_the_author_posts_link());
									?></p> -->
									<?php if(get_field('page_intro_text')): ?>
										<div class="featured">
											<?php echo get_field('page_intro_text'); ?>
										</div>
									<?php endif; ?>
									<?php if(get_field('page_intro_image')): ?>
										<div class="featured-image">
											<img src="<?php $image = get_field('page_intro_image'); echo $image['url']; ?>" alt="Content Banner Image" />
										</div>
									<?php endif; ?>

								</header>


							<section class="entry-content clearfix " itemprop="articleBody">
									<?php the_content(); ?>

									<?php 

									$taxonomies = array( 
									    'resource_category'
									);

									$cat_args = array(
									    'orderby'           => 'name', 
									    'order'             => 'ASC',
									    'hide_empty'        => true, 
									    'exclude'           => array(), 
									    'exclude_tree'      => array(), 
									    'include'           => array(),
									    'number'            => '', 
									    'fields'            => 'all', 
									    'slug'              => '', 
									    'parent'            => '',
									    'hierarchical'      => true, 
									    'child_of'          => 0, 
									    'get'               => '', 
									    'name__like'        => '',
									    'description__like' => '',
									    'pad_counts'        => false, 
									    'offset'            => '', 
									    'search'            => '', 
									    'cache_domain'      => 'core'
									); 
									$resource_cats = get_terms( $taxonomies );
										
								
									foreach ($resource_cats as $cat) { 
										// print_r($cat);
									}
								
 									?>

									<?php $dd_args = array(
										'show_option_all'    => '',
										'show_option_none'   => ' ',
										'orderby'            => 'ID', 
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 1, 
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => 0,
										'hierarchical'       => 1, 
										'name'               => 'resource_category_select',
										'id'                 => '',
										'class'              => 'postform',
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'resource_category',
										'hide_if_empty'      => false,
									); ?>

								<?php wp_dropdown_categories( $dd_args ); ?> 
							</section>

						<?php endwhile; endif; ?>

							<section class="law-practice-library" itemprop="articleBody">

							<?php foreach ($resource_cats as $cat) { ?>

								<?php
									
									$cat_name = $cat->name;
									$cat_url = get_term_link($cat);
									$is_sub = $cat->parent > 0;

								?>

								<div class="resource-group" id="Resource<?php echo $cat->term_id; ?>">

									<?php  if (!$is_sub) { ?>
										<div class="category-wrap">
											<h3><?php echo $cat_name; ?></h3>
										</div>
									<?php } ?>

									
									<?php
										$args = array( 'post_type' => 'resource', 'posts_per_page' => 50, 'resource_category' => $cat->slug );
										$loop = new WP_Query( $args );
									?>

									<?php if (have_posts()) { ?>
										<div class="legal-resources-wrap">

											<?php  while ( $loop->have_posts() ) : $loop->the_post(); ?>


												<div class="legal-resource">
													<h5><a href="<?php the_field('link'); ?>" target="_blank"><?php the_title(); ?></a></h5>
													<div class='desc'>
														<div class="excerpt">
															<?php the_field('description'); ?>
														</div>
													</div>
												</div>
											

											<?php endwhile; ?>
										</div>
									<?php } ?>
							
								</div>

							<?php } ?>

							</section>


	
						</div>


						<?php //get_sidebar(); ?>

						<!-- <div class="sidebar">
							<div class="widget dynamicSubpageWidget">
								<h4 class="widgettitle">Categories</h4>
								<ul>
									<?php 
										$args = array(
											'title_li'           => '',
											'taxonomy'           => 'resource_category'
										);
										wp_list_categories( $args ); 
									?>
								</ul>
							</div>
						</div> -->
				</div>

				<script src="<?php echo get_bloginfo('template_directory');?>/library/js/libs/chosen_v1.1.0/chosen.jquery.min.js"></script>

			</div>

<?php get_footer(); ?>
