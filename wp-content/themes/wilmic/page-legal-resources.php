<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="first clearfix" role="main">
						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<header class="article-header">

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
									<!-- <p class="byline vcard"><?php
										printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'bonestheme' ) ), bones_get_the_author_posts_link());
									?></p> -->
									<?php if(get_field('page_intro_text')): ?>
										<div class="featured">
											<?php echo get_field('page_intro_text'); ?>
										</div>
									<?php endif; ?>
									<?php if(get_field('page_intro_image')): ?>
										<div class="featured-image">
											<img src="<?php $image = get_field('page_intro_image'); echo $image['url']; ?>" alt="Content Banner Image" />
										</div>
									<?php endif; ?>

								</header>


							<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>

									<?php $args = array(
										'show_option_all'    => '',
										'orderby'            => 'ID',
										'order'              => 'ASC',
										'style'              => 'list',
										'show_count'         => 0,
										'hide_empty'         => 1,
										'use_desc_for_title' => 1,
										'child_of'           => 0,
										'feed'               => '',
										'feed_type'          => '',
										'feed_image'         => '',
										'exclude'            => '',
										'exclude_tree'       => '',
										'include'            => '',
										'hierarchical'       => 1,
										// 'title_li'           => __( 'Categories' ),
										'title_li'           => '',
										'show_option_none'   => __( 'No categories' ),
										'number'             => null,
										'echo'               => 1,
										'depth'              => 0,
										'current_category'   => 0,
										'pad_counts'         => 0,
										'taxonomy'           => 'resource_category',
										'walker'             => null
									); ?>

									<?php $dd_args = array(
										'show_option_all'    => '',
										'show_option_none'   => '',
										'orderby'            => 'ID', 
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 1, 
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => 0,
										'hierarchical'       => 0, 
										'name'               => 'cat',
										'id'                 => '',
										'class'              => 'postform',
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'resource_category',
										'hide_if_empty'      => false,
									); ?>
								<!-- <ul>
									<?php wp_list_categories( $args ); ?>
								</ul> -->
								<?php wp_dropdown_categories( $dd_args ); ?> 
							</section>

						<?php endwhile; endif; ?>


						<?php
							
							$args = array( 'post_type' => 'resource', 'posts_per_page' => 50 );
							$loop = new WP_Query( $args );
							
						?>
						<h3>All Resources</h3>
							<?php if (have_posts()) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

							<div class="legal-resource">
								<h4><?php the_title(); ?></h4>
								<div class='desc'>
									<div class="excerpt">
										<?php the_field('description'); ?>
									</div>
									<a href="<?php the_field('link'); ?>" target="_blank">Visit Site &rarr;</a>
								</div>
							</div>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
