<?php wp_reset_postdata(); ?>

<div id="sidebar1" class="sidebar clearfix" role="complementary">
	
	<!-- This is to offset the top of the searchbar the same height as the page title -->
	<!-- <h1 class='page-title'>&nbsp;</h1> -->

	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php else : ?>

		<?php // This content shows up if there are no widgets defined in the backend. ?>

		<div class="alert alert-help">
			<p><?php _e( 'Please activate some Widgets.', 'bonestheme' );  ?></p>
		</div>

	<?php endif; ?>

</div>