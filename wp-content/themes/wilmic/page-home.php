<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="home-main" class="first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<!-- <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1> -->
									<!-- <p class="byline vcard"><?php
										printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span>.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'F jS, Y', 'bonestheme' ) ), bones_get_the_author_posts_link());
									?></p> -->


								</header>

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>

									<?php if(get_field('action_blocks')): ?>
										<div id="ActionBlocks">
											<?php while(the_repeater_field('action_blocks')): ?>
												<?php $title = get_sub_field('title'); ?>
												<?php $content = get_sub_field('content'); ?>

												<div class="action-block">
													<div class="action-block-title">
														<h4><?php echo $title; ?></h4>
													</div>
													<div class="action-block-content">
														<?php echo $content; ?>
													</div>

													<?php if(get_sub_field('action_links')): ?>
														<div class="action-block-link">
														<?php while(the_repeater_field('action_links')): ?>
															<?php $link = get_sub_field('link'); ?>
															<?php $link_text = get_sub_field('link_text'); ?>
															<?php $p_or_s = get_sub_field('action_weight'); ?>
																<a class="action-link <?php echo $p_or_s; ?>" href="<?php echo $link; ?>">
																	<div class="text no-icon">
																		<div class="main_text"><?php echo $link_text; ?></div>
																		<div class="sub_text"></div>
																	</div>
																	<div class="clearfix"></div>
																</a>
														<?php endwhile; ?>
														</div>
													<?php endif; ?>
												</div>
										    <?php endwhile; ?>
										</div>
									<?php endif; ?>
									
									<div class="midpage-wrapper">
										<div class="midpage-notice aligncenter">
											<h3><?php echo get_bloginfo( 'description'); ?></h3>
										</div>
									</div>

									<div id="HomeAnnouncements" class="widget-area">
										<?php dynamic_sidebar( 'homepage_widgets' ); ?>
									</div>

								</section>

								<footer class="article-footer">
									<?php the_tags( '<span class="tags">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?>

								</footer>

								<!-- <?php comments_template(); ?> -->

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

				</div>

				<script src="<?php echo get_bloginfo('template_directory');?>/library/js/jquery.anyslider.min.js"></script>
				<script src="<?php echo get_bloginfo('template_directory');?>/library/js/imagesloaded.pkgd.min.js"></script>
				<script src="<?php echo get_bloginfo('template_directory');?>/library/js/masonry.pkgd.min.js"></script>
				
			</div>

<?php get_footer(); ?>
